import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
// import { SidebarComponent } from './sidebar/sidebar.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ContentComponent } from './content/content.component';
import { GridComponent } from './content/grid/grid.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { NavComponent } from './nav/nav.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator'; 
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import { ButtonAddComponent } from './content/button-add/button-add.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import { AjouterComponent } from './ajouter/ajouter.component';
import { DialogComponent } from './dialog/dialog.component';
import {ReactiveFormsModule, NgForm, FormsModule} from '@angular/forms'
import {MatFormFieldModule} from '@angular/material/form-field';
import {HttpClientModule}  from '@angular/common/http';
import { GridProjetComponent } from './content/grid-projet/grid-projet.component';
import { GridUserComponent } from './content/grid-user/grid-user.component';
import { AppRoutingModule } from './app-routing.module';
import { MatNativeDateModule, MatSnackBarModule } from '@angular/material';
import { UserServiceService } from './Service/User/user-service.service';
@NgModule({
  declarations: [
    AppComponent,
    // SidebarComponent,
    ToolbarComponent,
    ContentComponent,
    GridComponent,
    NavigationComponent,
    NavComponent,
    ButtonAddComponent,AjouterComponent,MyDialogComponent,DialogComponent, GridProjetComponent, GridUserComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatDialogModule,
    MatMenuModule,MatExpansionModule,ReactiveFormsModule,MatFormFieldModule,
    HttpClientModule,
    AppRoutingModule,MatNativeDateModule,MatSnackBarModule
  ],
  entryComponents:[
    MyDialogComponent
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
