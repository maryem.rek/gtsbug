import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridProjetComponent } from './content/grid-projet/grid-projet.component';
import { GridUserComponent } from './content/grid-user/grid-user.component';

const routes: Routes = [
  { path: '', redirectTo: 'listUtilisateur', pathMatch: 'full' },
  { path: 'listUtilisateur', component: GridUserComponent },
  { path: 'listProjet', component: GridProjetComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
