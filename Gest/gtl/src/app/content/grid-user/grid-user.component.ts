import {Component, OnInit, ViewChild, EventEmitter, Output} from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { UserServiceService } from 'src/app/Service/User/user-service.service';
import { User } from 'src/app/models/user';
import { MyDialogComponent } from 'src/app/my-dialog/my-dialog.component';


@Component({
  selector: 'app-grid-user',
  templateUrl: './grid-user.component.html',
  styleUrls: ['./grid-user.component.css']
})
export class GridUserComponent implements OnInit {
  
  @Output() addUserEvent: EventEmitter<any> = new EventEmitter()
  users: User[] = [];

  constructor(
    public dialog: MatDialog,
    private userService:UserServiceService
  ) {}

  ngOnInit() {
    this.userService.getAllUser().subscribe(res => {
      this.users = res as User[];
    });
  }
  
  onEditUser(user: User) {
    let dialogRef = this.dialog.open(MyDialogComponent, {
      data: user
    });
  }

  onDeleteUser(id: number) {
    this.userService.deleteUser(id)
      .subscribe(
        response => {
          this.users = this.users.filter( 
            (item) => item.id != id 
          );
        }
      )
  }

  getDate(){
    this.ngOnInit();
  }

}



