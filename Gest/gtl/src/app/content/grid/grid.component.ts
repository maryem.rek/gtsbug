import {Component, OnInit, ViewChild} from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BugService } from 'src/app/Service/Bug/bug.service';
import { Bug } from 'src/app/models/bug';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  displayedColumns: string[] = ['interface', 'type', 'date', 'status'];
  dataSource = new MatTableDataSource<Bug>();
  bugs:Bug[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  
  constructor(private bugService:BugService) { }

  
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.returnAllBugs();
  }

  deleteBug(id)
  {
   this.bugService.deleteBugs(id)
          .subscribe(() =>{
            this.bugs=this.bugs.filter(bug=> bug.idB !=id)
          })
  }

  returnAllBugs(){
    this.bugService.getAllBugs().subscribe(res => {
      this.bugs = res as Bug[];
      this.dataSource = new MatTableDataSource<Bug>(this.bugs);
    }); 
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  
}

const ELEMENT_DATA: PeriodicElement[] = [
  
];
