import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridProjetComponent } from './grid-projet.component';

describe('GridProjetComponent', () => {
  let component: GridProjetComponent;
  let fixture: ComponentFixture<GridProjetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridProjetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
