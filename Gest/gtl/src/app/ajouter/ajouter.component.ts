import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import { UserServiceService } from '../Service/User/user-service.service';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { User } from '../models/user';

@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.component.html',
  styleUrls: ['./ajouter.component.css']
})

export class AjouterComponent implements OnInit {
  
  @Input('user') selectedUser: User;
  @Output() addUserEvent: EventEmitter<any> = new EventEmitter()
  
  user: User;
  isUpdate: boolean = false;
  
  constructor(
    private userService: UserServiceService,
    private dialogRef:MatDialogRef<AjouterComponent>
  ) {}

  ngOnInit() {
    this.initUser();
    if(this.selectedUser) {
      this.user = this.selectedUser;
      this.isUpdate = true;
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  onSaveUser() {
    if(this.isUpdate) {
      this.userService.updateUser(this.user)
        .subscribe(
          response => {
            this.initUser();
            this.cancel();
          }
        )
    } else {
      this.userService.addUser(this.user)
        .subscribe(
          response => {
            this.addUserEvent.emit(response);
            this.initUser();
            this.cancel();
          }
        )
    }
  }

  // Helpers 
  initUser() {
    this.user = {id: null, nom: '', prenom: '', email: '', password: '', role: ''};
  }
}
