import { TestBed } from '@angular/core/testing';

import { DescriptionServiceService } from './description-service.service';

describe('DescriptionServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DescriptionServiceService = TestBed.get(DescriptionServiceService);
    expect(service).toBeTruthy();
  });
});
