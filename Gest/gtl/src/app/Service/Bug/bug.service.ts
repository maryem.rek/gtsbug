import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Bug } from '../../models/bug';
import { Observable } from 'rxjs';
const headr={
  headers:new HttpHeaders({
    'Content-Type':'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})
export class BugService {

  private baseUrl = 'http://localhost:9090/api/Bug';

  constructor(private http:HttpClient) { }

  getAllBugs():Observable<Bug[]>{
    return this.http.get<Bug[]>(this.baseUrl)
  }
  getOneBug(id:number):Observable<Bug>{
    return this.http.get<Bug>(`${this.baseUrl}/${id}`)
  }

  addBug(bug:Bug):Observable<any>{
    return this.http.post(this.baseUrl,bug,headr)
    
  }
  ubdateBugs(bug:Bug){
    return this.http.put(this.baseUrl,bug)
  }
  deleteBugs(id:number){
    return this.http.delete(`${this.baseUrl}/${id}`,{responseType:'text'})
  }
}
