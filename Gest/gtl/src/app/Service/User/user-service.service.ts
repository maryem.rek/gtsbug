import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserServiceService {
  formData:User;
  
  private urlUser: string = environment.apiUrl + "/user";
  
  constructor(private http:HttpClient) { }
  
  getAllUser(){
    return this.http.get(this.urlUser)
    
  }
  
  getOneUser(id:number){
    return this.http.get(`${this.urlUser}/${id}`)
  }
  
  addUser(user:User){
    return this.http.post(this.urlUser,user)   
  }
  
  updateUser(user:User){
    return this.http.put(this.urlUser,user)
  }
  
  deleteUser(id:number){
    return this.http.delete(`${this.urlUser}/${id}`,{responseType:'text'})
  }
}
