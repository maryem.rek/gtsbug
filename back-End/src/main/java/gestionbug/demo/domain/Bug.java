package gestionbug.demo.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Bug {

	@Id
	@GeneratedValue
	private Long idB;
	private String interfacee;
	private String type;
	private Date date;
	private String statut;

	@ManyToOne()
	private Projet projet;
	@OneToMany(mappedBy = "bug")
	private List<Description> descriptions;

	public Long getIdB() {
		return idB;
	}

	public void setIdB(Long idB) {
		this.idB = idB;
	}

	public String getInterfacee() {
		return interfacee;
	}

	public void setInterfacee(String interfacee) {
		this.interfacee = interfacee;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	@JsonIgnore
	public List<Description> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(List<Description> descriptions) {
		this.descriptions = descriptions;
	}

	public Bug(Long idB, String interfacee, String type, Date date, String statut, Projet projet,
			List<Description> descriptions) {
		this.idB = idB;
		this.interfacee = interfacee;
		this.type = type;
		this.date = date;
		this.statut = statut;
		this.projet = projet;
		this.descriptions = descriptions;
	}

	public Bug() {
	}
}
