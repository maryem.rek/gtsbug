package gestionbug.demo.rest;

import gestionbug.demo.domain.User;
import gestionbug.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("")
    public User add(@RequestBody User user)
    {
        return userService.add(user);
    }

    @GetMapping("/find/{id}")
    public User find(@PathVariable Long id){return  userService.find(id);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { userService.delete(id);}

    @PutMapping("")
    public User edite(@RequestBody User user){ return userService.edit(user);}

    @GetMapping("/search")
    public List<User> find(@RequestParam String nom){
        return userService.search(nom);
    }

    @GetMapping("")
    public List<User> getAll() {
        return userService.getAll();}


}
