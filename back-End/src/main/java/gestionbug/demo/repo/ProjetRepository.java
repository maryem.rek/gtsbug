package gestionbug.demo.repo;

import gestionbug.demo.domain.Projet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjetRepository extends JpaRepository<Projet,Long> {


    List<Projet> findBynom(String nom);
}
