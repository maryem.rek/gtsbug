package gestionbug.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gestionbug.demo.domain.User;
import gestionbug.demo.repo.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	public List<User> getAll() {

		return  userRepository.findAll();
	}

	public User add(User user) {
		if (user.getId() == null)
			return userRepository.save(user);
		return null;
	}

	public Optional<User> findOne(Long id) {
		return userRepository.findById(id);
	}

	public User find(Long id) {
		Optional<User> user = findOne(id);
		if (user.isPresent()) {
			return user.get();
		}
		return null;
	}

	public void delete(Long id) {
		User user = find(id);
		if (user != null)
			userRepository.delete(user);
	}

	public List<User> search(String nom) {
		return userRepository.findBynom(nom);
	}

	public User edit(User user) {
		if (user.getId() == 0 || find(user.getId()) == null) {
			return user;
		}
		return userRepository.save(user);
	}

}
